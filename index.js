var express = require('express');
var spawn = require('spawnify');
var bp = require('body-parser');
var app = express();

jsonparser = bp.json();
jobID = 0;

app.get('/', function (req, res) {
  res.send('Hello World!');
});

//build scheduler
app.post('/schedule', jsonparser, function (req, res) {

  console.log(req.body.time, req.body.tag, req.body.os, req.body.distro);
  //timeseconds,is_repeat,tagcommit,ostype,distro
  
  ls = spawn('ls','/home/faiz/', '-l');
  ls.on('data',function(d) {
     console.log('d',d);
  });
  console.log();
  res.send('Hello World!');
});

//slack receive hook
app.post('/slackhook', function (req, res) {
  //understands:
  // info - displays available commands
  // build args - start a new build
  //returns 'success' or errormsg
  res.send('Hello World!');
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
